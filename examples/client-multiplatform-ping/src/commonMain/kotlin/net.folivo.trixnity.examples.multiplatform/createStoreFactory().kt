package net.folivo.trixnity.examples.multiplatform

import net.folivo.trixnity.client.store.StoreFactory

expect fun createStoreFactory(): StoreFactory