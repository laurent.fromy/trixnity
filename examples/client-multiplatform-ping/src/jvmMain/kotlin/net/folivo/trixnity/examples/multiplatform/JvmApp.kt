package net.folivo.trixnity.examples.multiplatform

import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking { timelineExample() }
//    runBlocking { verificationExample() }
//    runBlocking { simpleBenchmark() }
}