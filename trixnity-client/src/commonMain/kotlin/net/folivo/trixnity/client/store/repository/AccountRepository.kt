package net.folivo.trixnity.client.store.repository

import net.folivo.trixnity.client.store.Account

typealias AccountRepository = MinimalStoreRepository<Long, Account>