package net.folivo.trixnity.client.store.repository

import net.folivo.trixnity.client.store.UploadMedia

typealias UploadMediaRepository = MinimalStoreRepository<String, UploadMedia>