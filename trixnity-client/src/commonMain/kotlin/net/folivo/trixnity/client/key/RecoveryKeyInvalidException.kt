package net.folivo.trixnity.client.key

class RecoveryKeyInvalidException(message: String) : RuntimeException(message)