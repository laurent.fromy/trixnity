package net.folivo.trixnity.appservice

data class MatrixAppserviceProperties(
    val hsToken: String,
)