package net.folivo.trixnity.appservice.user

data class RegisterUserParameter(
    val displayName: String? = null
)