package net.folivo.trixnity.clientserverapi.model.rooms

enum class ReceiptType(val value: String) {
    READ("m.read")
}