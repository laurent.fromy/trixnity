package net.folivo.trixnity.clientserverapi.model.authentication

enum class AccountType(val value: String) {
    GUEST("guest"),
    USER("user")
}