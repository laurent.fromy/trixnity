package net.folivo.trixnity.clientserverapi.model.media

enum class ThumbnailResizingMethod(val value: String) {
    CROP("crop"), SCALE("scale")
}