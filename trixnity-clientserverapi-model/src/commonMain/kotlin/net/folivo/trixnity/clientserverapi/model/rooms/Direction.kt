package net.folivo.trixnity.clientserverapi.model.rooms

enum class Direction(val value: String) {
    FORWARD("f"), BACKWARDS("b")
}